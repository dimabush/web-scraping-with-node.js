var graph = require('fbgraph');
var dbConnection = require('./dbConnection');


var fbUrl = "http://www.planetakino.ua https://www.facebook.com/IMAXUA https://www.instagram.com/planetakino/ https://vk.com/imaxua";
var res = function () {
    var arr = fbUrl.split(/\/| |\?/);
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].includes('facebook.com')) {
            //pg check..
            if (arr[i + 1] == 'pg') {
                return arr[a + 2]
            } else {
                return arr[i + 1]
            }
        }
    }
};
console.log(res());
var nameFb = res();


graph.get('/oauth/access_token?client_id=1533172393374061&client_secret=289bb5813f427687c79d441e2e21a988&grant_type=client_credentials', function (err, res) {
    // console.log(res.access_token);
    graph.setAccessToken(res.access_token);
    graph.get(nameFb, function (err, res) {
        console.log(res.id);
        var page_id = res.id;
        var objForCompany = {};
        var objForPosts = {};

        graph.get(page_id + '?fields=id,name,posts,emails,phone,website', function (err, res) {
            console.log('name: ' + res.name);
            objForCompany.name = res.name.replace(/'|"/g, "\\'");
            objForCompany.id = res.id;
            objForCompany.email = res.emails;
            objForCompany.phone = res.phone;
            objForCompany.website = res.website;

            dbConnection.insertIntoCompany(objForCompany);
            // console.log(res.posts.data);
            objForPosts.companyName = objForCompany.name;

            res.posts.data.forEach(function (element) {

                graph.get(element.id + '?fields=shares,likes.summary(true),comments.summary(true)', function (err, res) {
                    objForPosts.message = String(element.message).replace(/'|"/g, "\\'");
                    objForPosts.id = element.id;
                    objForPosts.created_time = element.created_time;
                    objForPosts.countOfLikes = res.likes.summary.total_count;
                    objForPosts.countOfComments = res.comments.summary.total_count;
                    if (res.shares) {
                        objForPosts.countOfShares = res.shares.count;

                    } else {
                        objForPosts.countOfShares = '0';
                    }

                    dbConnection.insertIntoPosts(objForPosts);
                });

            });

        });

    });

});

