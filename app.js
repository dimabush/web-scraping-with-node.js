var request = require('request');
var cheerio = require('cheerio');
var dbConnection = require('./dbConnection');
var tress = require('tress');

var URL = 'https://lviv.dityvmisti.ua/search/?w=Today';
var k = 0;

var q = tress(function (url, callback) {


    request(url, function (err, res, body) {
        if (err) throw err;

        var $ = cheerio.load(body);

        var objToInsert = {};

        //title
        var titleVar = $('.article__header').children().first().text().replace(/'|"/g, "\\'");
        objToInsert.title = titleVar;

        //image url
        var imageUrl = $('.text-content img').attr('src');
        objToInsert.imageUrl = imageUrl;

        //event description
        var textVar = $('.text-content center').nextUntil("hr").text().trim().replace(/'|"/g, "\\'");
        objToInsert.description = textVar;

        //other desc
        $('.text-content h3').each(function () {
            var content = $(this).next().text().trim().replace(/'|"/g, "\\'");
            switch ($(this).text()) {
                case 'Коли':
                    objToInsert.date = content;
                    break;
                case 'Вік':
                    objToInsert.age = content;
                    break;
                case 'Телефон':
                    objToInsert.phone = content.replace(/\s+/g, ' ');
                    break;
                case 'Адреса':
                    objToInsert.address = content.replace(/\s+/g, ' ');
                    break;
                case 'Посилання':
                    objToInsert.link = content.replace(/\s+/g, ' ');
                    break;

            }
        });


        if (objToInsert.title) {
            dbConnection.insertData(objToInsert);
        }


        $('.action-page').each(function () {
            if ($(this).attr('aria-label') == "Next") {
                var nextPageUrl = URL + "&p=" + $(this).attr('data-p');
                console.log('next page: ' + nextPageUrl);
                // q.push(nextPageUrl); //uncomment to take data from all pages
            }
        });

        $('li[class=m-event-list__item]').each(function () {
            var newhref = $(this).children().find('a').attr('href');
            var eventUrl = 'https://lviv.dityvmisti.ua' + newhref;

            var j = ++k;
            console.log(j + ': ' + eventUrl);
            q.push(eventUrl);
        });


        callback();
    });
}, 10);

q.drain = function () {
    console.log('DONE!');
};

q.push(URL);