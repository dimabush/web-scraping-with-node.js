var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root", //enter username
    password: "root", //enter password
    database: "mydbtest", //enter name of DB
    charset: "utf8mb4"
});

con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");

    con.query('CREATE TABLE IF NOT EXISTS dityvmisti (' +
        'event_id INT NOT NULL AUTO_INCREMENT, ' +
        'title VARCHAR(100), ' +
        'img_url VARCHAR(100), ' +
        'description TEXT, ' +
        'date VARCHAR(45), ' +
        'age VARCHAR(45), ' +
        'phone VARCHAR(45), ' +
        'address VARCHAR(45), ' +
        'links TEXT, ' +
        'PRIMARY KEY ( event_id ));', function (err, result) {
        if (err) throw err;
        console.log("Result from creating tblWithEvents: " + result);
    });
    con.query('CREATE TABLE IF NOT EXISTS company (' +
        'id INT NOT NULL AUTO_INCREMENT, ' +
        'name VARCHAR(45), ' +
        'fb_id VARCHAR(45), ' +
        'phone VARCHAR(45), ' +
        'email VARCHAR(45), ' +
        'website VARCHAR(45), ' +
        'PRIMARY KEY ( id ));', function (err, result) {
        if (err) throw err;
        console.log("Result from creating tblCompany: " + result);
    });
    con.query('CREATE TABLE IF NOT EXISTS posts (' +
        'id INT NOT NULL AUTO_INCREMENT, ' +
        'text TEXT, ' +
        'post_fb_id VARCHAR(45), ' +
        'date VARCHAR(45), ' +
        'count_of_likes VARCHAR(45), ' +
        'count_of_shares VARCHAR(45), ' +
        'count_of_comments VARCHAR(45), ' +
        'company_id INT, ' +
        'PRIMARY KEY ( id ));', function (err, result) {
        if (err) throw err;
        console.log("Result from creating tblPosts: " + result);
    });
});


function insertData(tempObj) {
    var query = 'INSERT INTO dityvmisti (title, img_url, description, date, age, phone, address, links) VALUES (\'' + tempObj.title + '\', \'' + tempObj.imageUrl + '\', \'' + tempObj.description + '\', \'' + tempObj.date + '\', \'' + tempObj.age + '\', \'' + tempObj.phone + '\', \'' + tempObj.address + '\', \'' + tempObj.link + '\');';
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log("Result from insert in Events Table: " + result);
    });
    console.log(query + '\n');
}

function insertInCompany(objToInsert) {
    var query = 'INSERT INTO company (name, fb_id, phone, email, website) VALUES (\'' + objToInsert.name + '\', \'' + objToInsert.id + '\', \'' + objToInsert.phone + '\', \'' + objToInsert.email + '\', \'' + objToInsert.website + '\');';
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log("Result from insert in Company Table: " + result);
    });
    console.log(query + '\n');
}

function insertInPosts(objToInsert) {
    // var query = 'INSERT INTO posts (text, post_id, date) VALUES (\'' + objToInsert.message + '\', \'' + objToInsert.id + '\', \'' + objToInsert.created_time + '\');';
    var query = 'INSERT INTO posts (text, post_fb_id, date, count_of_likes, count_of_shares, count_of_comments, company_id) SELECT \'' + objToInsert.message + '\', \'' + objToInsert.id + '\', \'' + objToInsert.created_time + '\', \'' + objToInsert.countOfLikes + '\', \'' + objToInsert.countOfShares + '\', \'' + objToInsert.countOfComments + '\', id FROM company WHERE name = \'' + objToInsert.companyName + '\';';
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log("Result from insert in Company Table: " + result);
    });
    console.log(query + '\n');
}


var moduleManager = {
    insertData: function (tempObject) {
        return insertData(tempObject);
    },
    insertIntoCompany: function (insObject) {
        return insertInCompany(insObject)
    },
    insertIntoPosts: function (insObject) {
        return insertInPosts(insObject)
    }
};

module.exports = moduleManager;
